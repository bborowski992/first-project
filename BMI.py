# BMI = waga / wzrost **2
waga = float(input("Podaj swoją wagę w kg:\n"))
wzrost = float(input("Podaj swój wzrost:\n"))
wzrost = wzrost / 100
BMI = waga / wzrost ** 2
print("Twoje BMI wynosi: ",BMI)

if BMI < 18.5:
    print("Niedowaga")
elif BMI >= 18.5 and BMI <= 24:
    print("Waga normalna")
elif BMI >= 24 and BMI <= 26.5:
    print("Lekka nadwaga")
elif BMI >= 26.5 and BMI <= 29.5:
    print("Nadwaga")
elif BMI >= 30 and BMI <= 35:
    print("Otyłość I stopnia")
elif BMI >= 35 and BMI <= 40:
    print("Otyłość II stopnia")
else:
    print("Otyłość III stopnia")
